# Overview #

* [Instalation](https://bitbucket.org/ivanmijatovic89/josh/overview#markdown-header-instalation)
* [Installed Packages](https://bitbucket.org/ivanmijatovic89/josh/overview#markdown-header-packages)
* *  [Translatable](https://bitbucket.org/ivanmijatovic89/josh/overview#markdown-header-translatable)
* *  [Ide Helper](https://bitbucket.org/ivanmijatovic89/josh/overview#markdown-header-ide-helper)
* *  [Iseed](https://bitbucket.org/ivanmijatovic89/josh/overview#markdown-header-iseed)
* *  [Debuger](https://bitbucket.org/ivanmijatovic89/josh/overview#markdown-header-debuger)
* *  [Modules](https://bitbucket.org/ivanmijatovic89/josh/overview#markdown-header-modules)
* *  [CRUD Generator](https://bitbucket.org/ivanmijatovic89/josh/overview#markdown-header-crud-generator)

# Instalation

1. Pull or clone code

### Database
Incase, you don't find it in root folder of your laravel installation, please rename `.env.example` to `.env` and then edit database, environment related details in that file.

~ Setup database in `.env` file ~

### Storage Permissions
You'll need to make sure that the storage directory is writable by your webserver, since caches and log files get written there. You should use the minimum permissions available for writing, based on how you've got your webserver configured.

```php
chmod -R 755 storage
```
### Upload directory permissions

User's profile pics will be uploaded into `public/uploads/users`so we need to provide write access for that folder to do so, please run following command in your command prompt/terminal

```php
chmod 777 public/uploads/users
```

### Mail
Enter your mail settings in `config/mail.php`
```php
config/mail.php
```

###  Database Migration and Seeds

Migrations -  `database\migrations` to know what fields are being added. Seeds -         `database\seeds`

```php
php artisan migrate --seed
```

**Admin**

>u: admin@admin.com

>p: password

**User**

>u: user@user.com

>p: password

Senty config in `config/sentry` please go to [Sentry website](https://cartalyst.com/manual/sentry/2.1) to learn more about it

You can change this credidentals in `database/seeds/AdminSeeder.php`


# That's it You are all set ! Now go make some awsome apps !

```php
# Login at
domain.com/admin
```

[Josh Admin Template Documentation](http://lorvent.gitbooks.io/josh/content/laravel_5_version.html)


# Packages

### Translatable
[repo](https://github.com/dimsav/laravel-translatable)

Translatable Config file you can find in:
```php
app/config/translatable.php
```

### Ide Helper
[repo](https://github.com/barryvdh/laravel-ide-helper)

Generate ide helper
```php
// it will create file in root -    /_ide_helper.php
php artisan ide-helper:generate

// PhpStorm Meta for Container instances
php artisan ide-helper:meta
```

### Iseed

[repo](https://github.com/orangehill/iseed)

You can run Iseed from the command line using Artisan, e.g. `php artisan iseed users`

Or you can run for multiple tables e.g.  `php artisan iseed users,groups,users_groups`

If you wish to overwrite it by default use `--force`   e.g. `php artisan iseed users --force`

To generate a seed file for your users table simply call: `\Iseed::generateSeed('users', 'connectionName', 'numOfRows');`. connectionName and numOfRows are not required arguments.

This will create a file inside a `/database/seeds`



### Debuger

[repo](https://github.com/barryvdh/laravel-debugbar)

Turn off / on in config file located `app/config/debugbar.php`


### Modules

[repo](https://github.com/caffeinated/modules)

[documentation](https://github.com/caffeinated/modules/wiki/Artisan-Commands)

Create Module, e.g. blog:
```php
php artisan module:make blog
```

Module List:
```php
php artisan module:list
```

Disable Module:
```php
php artisan module:disable blog
```

Enable Module:
```php
php artisan module:enable blog
```



### CRUD Generator

[repo](https://github.com/ivanmijatovic89/proto-maker-generator)

to make a Crud go to `domain/protomaker`
