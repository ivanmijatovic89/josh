<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('users')->delete();
        
		\DB::table('users')->insert(array (
			0 => 
			array (
				'id' => 1,
				'email' => 'admin@admin.com',
				'password' => '$2y$10$PEm8K.qKecdw4Syzmec7Q.N44k9TEEXN2ICN6UGoPdjWoLnG/HkcS',
				'permissions' => NULL,
				'activated' => 1,
				'activation_code' => NULL,
				'activated_at' => NULL,
				'last_login' => '2015-03-26 21:07:33',
				'persist_code' => '$2y$10$xJw7lM.flg951ZooalPZ3u5L0njOJy2ZxtquwyMhHSU0EXFo0umaW',
				'reset_password_code' => NULL,
				'first_name' => 'John',
				'last_name' => 'Doe',
				'created_at' => '2015-03-26 21:05:34',
				'updated_at' => '2015-03-26 21:07:33',
				'deleted_at' => NULL,
				'bio' => NULL,
				'gender' => NULL,
				'dob' => NULL,
				'pic' => NULL,
				'country' => NULL,
				'state' => NULL,
				'city' => NULL,
				'address' => NULL,
				'postal' => NULL,
			),
			1 => 
			array (
				'id' => 2,
				'email' => 'user@user.com',
				'password' => '$2y$10$E9r.wfTMPvWqnplDGvV4G.IY3MyO7y8VcmA9KyPxfKm4100Wvtxgi',
				'permissions' => NULL,
				'activated' => 1,
				'activation_code' => NULL,
				'activated_at' => NULL,
				'last_login' => NULL,
				'persist_code' => NULL,
				'reset_password_code' => NULL,
				'first_name' => 'John',
				'last_name' => 'Doe',
				'created_at' => '2015-03-26 21:05:34',
				'updated_at' => '2015-03-26 21:05:34',
				'deleted_at' => NULL,
				'bio' => NULL,
				'gender' => NULL,
				'dob' => NULL,
				'pic' => NULL,
				'country' => NULL,
				'state' => NULL,
				'city' => NULL,
				'address' => NULL,
				'postal' => NULL,
			),
			2 => 
			array (
				'id' => 3,
				'email' => 'ivanmijatovic89@gmail.com',
				'password' => '$2y$10$OhJhC4NpSHFeI0dAG7Z3ZuS71xZrzc0cxp2clmQUtcTpHEGkNlpEa',
				'permissions' => NULL,
				'activated' => 1,
				'activation_code' => NULL,
				'activated_at' => NULL,
				'last_login' => '2015-03-26 22:13:29',
				'persist_code' => '$2y$10$6HBU3xY532ddvsO6AidTKuux4XiwMQEGm7rUR0dZRsxaKDuM0/qEW',
				'reset_password_code' => NULL,
				'first_name' => 'Ivan',
				'last_name' => 'Mijatovic',
				'created_at' => '2015-03-26 22:13:21',
				'updated_at' => '2015-03-26 22:13:29',
				'deleted_at' => NULL,
				'bio' => 'Web Developer',
				'gender' => 'male',
				'dob' => '1989-06-17',
				'pic' => '',
				'country' => 'RS',
				'state' => 'Serbia',
				'city' => 'Belgrade',
				'address' => 'Mestroviceva',
				'postal' => '11000',
			),
		));
	}

}
