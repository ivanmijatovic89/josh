<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('groups')->delete();
        
		\DB::table('groups')->insert(array (
			0 => 
			array (
				'id' => 1,
				'name' => 'Admin',
				'permissions' => '{"admin":1}',
				'created_at' => '2015-03-26 21:52:18',
				'updated_at' => '2015-03-26 21:52:18',
			),
			1 => 
			array (
				'id' => 2,
				'name' => 'User',
				'permissions' => '',
				'created_at' => '2015-03-26 21:52:18',
				'updated_at' => '2015-03-26 21:52:18',
			),
		));
	}

}
