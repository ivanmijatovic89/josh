<?php

use App\Models\User;

class AdminSeeder extends DatabaseSeeder {

	public function run()
	{
		DB::table('users')->truncate(); // Using truncate function so all info will be cleared when re-seeding.
		DB::table('groups')->truncate();
		DB::table('users_groups')->truncate();

		// Create Admin User
		Sentry::getUserProvider()->create(array(
			'email'       => 'admin@admin.com',
			'password'    => "password",
			'first_name'  => 'John',
			'last_name'   => 'Doe',
			'activated'   => 1,
		));

		// Create User
		Sentry::getUserProvider()->create(array(
			'email'       => 'user@user.com',
			'password'    => "password",
			'first_name'  => 'John',
			'last_name'   => 'Doe',
			'activated'   => 1,
		));
		// Create Admin Group
		Sentry::getGroupProvider()->create(array(
			'name'        => 'Admin',
			'permissions' => array('admin' => 1),
		));
		// Create User Group
		Sentry::getGroupProvider()->create(array(
			'name'        => 'User',
			'permissions' => array('admin' => 0),
		));

		// Assign admin user to Admin Group permissions
		$adminUser  = Sentry::getUserProvider()->findByLogin('admin@admin.com');
		$adminGroup = Sentry::getGroupProvider()->findByName('Admin');
		$adminUser->addGroup($adminGroup);

		// Assign  user to User Group permissions
		$regularUser  = Sentry::getUserProvider()->findByLogin('user@user.com');
		$userGroup    = Sentry::getGroupProvider()->findByName('User');
		$regularUser->addGroup($userGroup);
	}

}